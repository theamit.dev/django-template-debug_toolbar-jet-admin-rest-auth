## This document states how ot use this bolilerplate

#### _it has_ 

* debug_toolbar
* django_extensions
* rest_framework
* rest_framework.authtoken
* allauth
* allauth.account
* est_auth
* rest_auth.registration


#### _All the urls is set as per information below_
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', admin.site.urls),
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('rest_auth.urls')),
    url(r'^auth/registration/', include('rest_auth.registration.urls')),
### command

~~~~
mkvirtualenv new_name

pip install django
django-admin startproject --template=https://gitlab.com/sgos/django-template-debug_toolbar-jet-admin/repository/archive.zip?ref=django-alluth new_name

django-admin startproject --template=https://gitlab.com/sgos/django-template-debug_toolbar-jet-admin-rest-auth/repository/archive.zip?ref=django-alluth new_name


~~~~

![](http://spliceglobal.com/images/logo-icon.png)

© [SPLICE GLOBAL SERVICES PRIVATE LIMITED](http://www.spliceglobal.com)
