# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class ModelMixin(models.Model):
    """
    This mixins provide the default field in the models project wise
    """

    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_created")
    updated_by = models.ForeignKey(User, related_name="%(app_label)s_%(class)s_updated")

    class Meta:
        abstract = True
