#### __add followinf in application urls.py__ 

~~~
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from reminder.views import ListViewSet, ProductViewSet, ReminderViewSet

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'products', ProductViewSet)
router.register(r'lists', ListViewSet)
router.register(r'reminders', ReminderViewSet)
~~~


#### __add in main urls urls.py__

~~~
from utils.routers import DefaultRouter
from reminder.urls import router as reminder_router

router = DefaultRouter()
router.extend(reminder_router)

urlpatterns = [
    ...
    url(r'^api/', include(router.urls)),
]
~~~