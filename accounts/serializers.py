from rest_framework import serializers
from rest_auth.registration.serializers import RegisterSerializer as DefaultRegisterSerializer
from allauth.utils import email_address_exists, get_username_max_length
from allauth.account import app_settings as allauth_settings

from accounts.models import UserProfile


class RegisterSerializer(DefaultRegisterSerializer):
    """
    
    """
    username = serializers.CharField(
        max_length=get_username_max_length(),
        min_length=allauth_settings.USERNAME_MIN_LENGTH,
        required=allauth_settings.USERNAME_REQUIRED
    )
    email = serializers.EmailField(required=allauth_settings.EMAIL_REQUIRED)
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    mobile = serializers.CharField(max_length=15)

    def custom_signup(self, request, user):
        mobile = self.validated_data.get('mobile', '')
        user_profile = UserProfile(user=user, mobile=mobile)
        user_profile.save()

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'mobile': self.validated_data.get('mobile', ''),
        }
