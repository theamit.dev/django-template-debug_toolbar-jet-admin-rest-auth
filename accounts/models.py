# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    """
    
    """
    user = models.ForeignKey(User, related_name='profile')
    mobile = models.CharField(max_length=15, null=True, blank=True, db_index=True)

    def __str__(self):
        return str(self.mobile)
